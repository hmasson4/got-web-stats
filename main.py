# GAME OF THRONES STATS SCRAPER!
# BY: Harry and Claire Masson
# 2017/07/10

# Import requests and beautiful soup library
import requests
from bs4 import BeautifulSoup
import csv

# Set URL to scrape
url = 'https://en.wikipedia.org/wiki/Game_of_Thrones'

# Scrape the URL, process the results with BeautifulSoup
results = requests.get(url)
content = results.content # results['content']
soup = BeautifulSoup(content, 'html.parser')

# Look through tables in processed results, find table we want
# (table will have class 'wikitable' and have caption subelement
# with italics text in caption)
possible_tables = []
for t in soup.find_all("table", class_ = 'wikitable'):
    if t.caption:
        if t.caption.i:
            possible_tables.append(t)

# Should have only found one table in our list, select that table
stats_table = possible_tables[0]

# Process table by moving stripped contents into 2d list
stats = []

# Go through each "tr" row in the table
for row in stats_table.find_all("tr"):
    # Make a clean list for the stats pulled out in the row
    stats_row = []

    # Process each element in the row
    for elem in row.children:

        # Only care about 'td' and 'th' elements
        if elem.name == 'td' or elem.name == 'th':

            # Ignore elements that don't have any content
            if elem.contents:

                # Create a clean string we can use to add all content together
                c = ""

                # Process content of all immediate content elements
                for subcontent in elem.contents:

                    # Ignore 'sup' content - this is used by Wikipedia for reference links
                    # (and we don't care about this)
                    if subcontent.name != 'sup':
                        c += subcontent.string

                # Processing of subelements complete, add subelement text to the row
                stats_row.append(c)

    # Append the processed row to the stats list
    stats.append(stats_row)

# # Print the table nicely
# for row in stats:
#     for elem in row:
#         print(elem, end=" ")
#     print()
#
#
# print()
# print(stats[3][6])

# Set export filename
filename = 'output.csv'

# Export data as CSV
with open(filename, 'w') as csvfile:
    csvwriter = csv.writer(csvfile, delimiter=',', quoting=csv.QUOTE_NONE)
    for row in stats:
        csvwriter.writerow(row)

    print("Scraped and exported successfully!")